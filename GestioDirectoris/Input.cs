using System;
using System.IO;

namespace GestioDirectoris
{
    /// <summary>
    /// Métodos para consola de Practica de Gestión de directorios
    /// </summary>
    public class Input
    {
        /// <summary>
        /// Método para ejecutar por consola: WhatAndWhereIs().
        /// Comprobar si existe o no: un archivo o un directorio dado. Además muestra su ruta absoluta.
        /// </summary>
        public void ConsoleWhatAndWhereIs()
        {
           Utilities.WhereIam();
            string item;
            do
            {
                Console.Write("\n Introduce el nombre de un fichero o carpeta: ");
                item = Console.ReadLine();
            } while (item == null);
            Utilities.WhatAndWhereIs(item);
        }

        /// <summary>
        /// Método para ejecutar por consola: DirectoryContent()
        /// Mostrar por consola el contenido de un directorio
        /// </summary>
        public void ConsoleDirectoryContent()
        {
            Utilities.WhereIam();
            string dir;
            do
            {
                Console.Write("\n Introduce el nombre de un directorio: ");
                dir = Console.ReadLine();
            } while (dir == null);
            Utilities.DirectoryContent(dir);
        }
        
        /// <summary>
        /// Método para ejecutar por consola: CreateDirectory(), RemoveDirectory(), RenameDirectory()
        /// Crear, borrar o modificar el nombre de un directorio
        /// </summary>
        public void ConsoleOptionsDir()
        {
            Utilities.WhereIam();
            string dir;
            string option;
            (option, dir) = InsertOptionDir();
            
            switch (option.ToLower())
            {
                case "salir":
                    break;
                case "mkdir":
                    Utilities.CreateDirectory(dir);
                    break;
                case "rmdir":
                    Utilities.RemoveDirectory(dir);
                    break;
                case "rndir":
                    var newDir = NewNameDir();
                    Utilities.RenameDirectory(dir, newDir);
                    break;
            }
        }

        /// <summary>
        /// Método para ejecutar por consola: DirectoryAndSubdirectoryContent()
        /// Mostrar el contenido de un directorio y subdirectorios
        /// </summary>
        public void ConsoleDirectoryAndSubdirectoryContent()
        {
            Utilities.WhereIam();
            string dir;
            do
            {
                Console.Write("\n Introduce el nombre de un directorio: ");
                dir = Console.ReadLine();
            } while (dir == null || !Directory.Exists(dir));
            Utilities.DirectoryAndSubdirectoryContent(dir);
        }
        
        
        private static (string, string) InsertOptionDir()
        {
            string varInput;
            string dir = null;
            string option = null;
            while (option != "mkdir" && option != "rmdir" && option != "rndir")
            {
                do
                {
                    Console.Write("\n Introduce una opción acompañada del nombre del directorio: " +
                                  "\n - mkdir (crear)" +
                                  "\n - rmdir (borrar)" +
                                  "\n - rndir (renombrar)" +
                                  "\n Introduce salir para finalizar el programa" +
                                  "\n\n · ");
                    varInput = Console.ReadLine();
                } while (varInput == null || varInput.Length < 5);
                for (var i = 0; i <= 4; i++) option += varInput[i];
                for (var i = 6; i < varInput.Length; i++) dir += varInput[i];
            }
            return (option, dir);
        }
        
        private static string NewNameDir()
        {
            string newDir;
            do
            {
                Console.Write("\n Introduce el nuevo nombre del directorio: ");
                newDir = Console.ReadLine();
            } while (newDir == null);
            return newDir;
        }
    }
}