﻿using System;
using System.IO;
using System.Linq;

namespace GestioDirectoris
{
    /// <summary>
    /// Métodos estáticos para realizar los ejercicios
    /// </summary>
    public class Utilities
    {
        /// <summary>
        /// Método para mostrar el directorio actual
        /// </summary>
        public static void WhereIam()
        {
            var now = Environment.CurrentDirectory;
            Console.WriteLine("\n\n El directorio actual es: {0}\n", now);
        }


        /// <summary>
        /// Método para comprobar si existe o no: un archivo o un directorio dado. Además muestra su ruta absoluta.
        /// </summary>
        /// <param name="item">
        /// Posible directorio o archivo por comprobar.
        /// </param>
        public static void WhatAndWhereIs(string item)
        { 
            var fullPath = Path.GetFullPath(item);
            if (Directory.Exists(fullPath) || File.Exists(fullPath))
            {
                Console.Write("\n Esta es su ruta absoluta: {0} \n\n", fullPath);
                if (Directory.Exists(fullPath)) Console.Write(" Es un directorio");
                if (File.Exists(fullPath)) Console.Write(" Es un archivo");
            }
            else Console.Write("\n\t No existe");
        }

        /// <summary>
        /// Método para listar el contenido de un directorio
        /// </summary>
        /// <param name="dir">
        /// Path del directorio 
        /// </param>
        public static void DirectoryContent(string dir)
        {
            WhereIam();
            var dirInfo = new DirectoryInfo(dir);
            var dirs = dirInfo.EnumerateDirectories();
            Console.Write("\n\t Directorios: ");
            foreach (var n in dirs) Console.Write("\n - " + n);
            var txtFiles = dirInfo.EnumerateFiles();
            Console.Write("\n\n\t Archivos: ");
            foreach (var n in txtFiles) Console.Write("\n - " + n);
        }

        /// <summary>
        /// Método para crear un directorio
        /// </summary>
        /// <param name="dir">
        /// Directorio que debe ser creado 
        /// </param>
        public static void CreateDirectory(string dir)
        {
            if (Directory.Exists(dir)) Console.Write("\n El directorio {0} ya existe", dir);
            else
            {
                Directory.CreateDirectory(dir);
                Console.Write("\n Se ha creado el directorio: {0}", dir);
            }
        }

        /// <summary>
        /// Método para borrar un directorio
        /// </summary>
        /// <param name="dir">
        /// Directorio que debe ser borrado
        /// </param>
        public static void RemoveDirectory(string dir)
        {
            if (Directory.Exists(dir))
            {
                var dirEmptyOrNot = Directory.EnumerateFileSystemEntries(dir);
                if (!dirEmptyOrNot.Any())
                {
                    Directory.Delete(dir);
                    Console.Write("\n Se ha borrado el directorio: {0}", dir);
                }
                else
                {
                    string option;
                    do
                    {
                        Console.Write("\n El directorio no está vacio ¿Seguro que quieres eliminarlo? ");
                        option = Console.ReadLine();
                    } while (option != "si" && option != "no");
                    switch (option)
                    {
                        case "si":
                            Console.Write("\n Se ha borrado el directorio: {0}", dir);
                            Directory.Delete(dir, true);
                            break;
                        case "no":
                            Console.Write("\n El directorio no ha sido eliminado");
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Método para renombar un directorio
        /// </summary>
        /// <param name="oldDir">
        /// Path del antiguo directorio
        /// </param>
        /// <param name="newDir">
        /// Nuevo nombre del directorio
        /// </param>
        public static void RenameDirectory(string oldDir, string newDir)
        {
            if (Directory.Exists(oldDir))
            {
                Directory.Move(oldDir, newDir);
                Console.Write("\n Se ha renombrado el directorio: {0} a {1}", oldDir, newDir);
            }
            else
            {
                Directory.CreateDirectory(newDir);
                Console.Write("\n Se ha creado el directorio: {0}", newDir);
            }        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dir">
        ///
        /// </param>
        public static void DirectoryAndSubdirectoryContent(string dir)
        {
            if (Directory.Exists(dir))
            {
                var dirs = Directory.EnumerateDirectories(dir);
                var dirsList = dirs.ToList();
                if (dirsList.Any())
                {
                    foreach (var info in dirsList)
                    {
                        Console.Write("\n Directorio: " + info);
                        DirectoryAndSubdirectoryContent(info);
                    }
                }

                var files = Directory.EnumerateFiles(dir);
                var filesList = files.ToList();
                if (filesList.Any())
                {
                    foreach (var info in filesList)
                    {
                        Console.Write("\n Archivo: " + info);
                    }
                }
            }
        }
    }
}