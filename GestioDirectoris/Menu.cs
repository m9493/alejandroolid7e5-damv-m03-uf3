using System;
namespace GestioDirectoris
{
    /// <summary>
    /// Métodos para Menu
    /// </summary>
    public static class Menu
    {
        /// <summary>
        /// Método para ejecutar el menu
        /// </summary>
        public static void RunMenu()
        {
            var menu = true;
            while (menu)
            {
                Console.Write("\n INDICE DE EJERCICIOS:" +
                              "\n 01: Comprobar si existe un fichero o directorio" +
                              "\n 02: Listar el contenido de un directorio" +
                              "\n 03: Crear, borrar o renombrar un directorio" +
                              "\n 04: Listar el contenido de un directorio y sus subdirectorios" +
                              "\n Introduce 'salir' para finalizar el programa" +
                              "\n\n Introduce un ejercicio: ");
                var option = Console.ReadLine();
                if (option != null) option = option.ToLower();
                Console.Clear();
                var input = new Input();
                switch (option)
                {
                    default:
                        Console.WriteLine("Opción Incorrecta");
                        RunMenu();
                        break;
                    case "1":
                        input.ConsoleWhatAndWhereIs();
                        break;
                    case "2":
                        input.ConsoleDirectoryContent();
                        break;
                    case "3":
                        input.ConsoleOptionsDir();
                        break;
                    case "4":
                        input.ConsoleDirectoryAndSubdirectoryContent();
                        break;
                    case "salir":
                        menu = false;
                        break;
                }
                if (option != "salir") Console.ReadKey();
                Console.Clear();
            }
        }
    }
}